class Code
  attr_reader :pegs
  PEGS = {
    r: "Red",
    g: "Green",
    b: "Blue",
    y: "Yellow",
    o: "Orange",
    p: "Purple"
  }

  def initialize(pegs)
    @pegs = pegs.map(&:downcase)
  end

  def self.parse(string)
    pegs = string.chars.map do |char|
      unless PEGS.keys.include?(char.downcase.to_sym)
        raise "Error, Try again."
      end
      char.downcase
    end
    Code.new(pegs)
  end

  def self.random
    colors = "rgbyop".chars
    pegs = []

    4.times { pegs << colors.sample }

    Code.new(pegs)
  end

  def [](pos)
    pegs[pos]
  end

  def exact_matches(other_code)
    count = 0

    pegs.each_with_index do |peg, i|
      count += 1 if peg == other_code[i].downcase
    end
    count
  end

  def near_matches(other_code)
    set1 = @pegs.dup
    set2 = other_code[0..-1].dup
    count = 0

    set1.each do |peg1|
      set2.each_with_index do |peg2, idx|
        if peg1 == peg2
          count += 1
          set2.delete_at(idx)
          break
        end
      end
    end
    [count - exact_matches(other_code), 0].max
  end

  def ==(other_code)
    return false unless other_code.instance_of?(Code)
    exact_matches(other_code) == 4
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = nil)
    # Specified in specs that code should be array, not object
    @secret_code = code.nil? ? Code::random : Code.new(code[0..-1])
    @turns = 1
  end

  def play
    puts "\nMastermind v.0.0.1\n-------------------\n"
    puts "Available colors: (R)ed, (G)reen, (B)lue, (Y)ellow, (O)range, (P)urple"
    loop do
      puts "\nTurn #{@turns} (#{10 - @turns} left)"
      code = get_guess
      if @secret_code == code
        puts "\nThat's correct! You win!\n\n"
        break
      else
        puts
        display_matches(code)
      end
      @turns += 1
      if @turns > 10
        puts "\nTime's up! You lose!\n\n"
        break
      end
    end
  end

  def get_guess
    input = ""
    print "Please enter your guess: "
    loop do
      input = gets.chomp
      if input.length != 4
        print "Invalid input. Please try again: "
      else
        break
      end
    end
    Code.new(input.chars)
  end

  def display_matches(code)
    puts "exact matches: #{@secret_code.exact_matches(code)}"
    puts "near matches: #{@secret_code.near_matches(code)}"
  end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
